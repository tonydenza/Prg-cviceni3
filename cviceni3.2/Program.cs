﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni3._2
{
    class Program
    {
        class BinTree
        {
            public int data;
            public BinTree left;
            public BinTree right;
        }

        static BinTree Create(int number)
        {
            BinTree tree = new BinTree();
            tree.data = number;
            tree.left = null;
            tree.right = null;
            return tree;
        }

        static BinTree Insert(BinTree root, int number)
        {
            BinTree tmp = Create(number);
            BinTree act = root;

            if (root == null) return tmp;

            while (act != null)
            {
                if (act.data == number)
                    return root;

                if (number < act.data)
                {
                    if (act.left == null)
                        act.left = tmp;
                    else
                        act = act.left;
                }
                else
                {
                    if (act.right == null)
                        act.right = tmp;
                    else
                        act = act.right;
                }
            }
            return root;
        }

        static BinTree ConvertArray(int[] array)
        {
            BinTree tree = null;
            for (int i = 0; i < array.Length; i++)
                tree = Insert(tree, array[i]);
            return tree;
        }

        static BinTree Find(BinTree root, int number)
        {
            BinTree act = root;

            while (act != null)
            {
                if (act.data == number)
                    return act;

                if (number < act.data)
                    act = act.left;
                else
                    act = act.right;
            }

            return null;
        }

        static void PrintRec(BinTree root, int depth)
        {
            if (root == null) return;
            for (int i = 0; i < depth; i++) Console.Write("*");
            Console.WriteLine(root.data);
            PrintRec(root.left, depth + 1);
            PrintRec(root.right, depth + 1);
        }

        static void Print(BinTree root)
        {
            int depth = 0;
            PrintRec(root, depth);
            Console.WriteLine();
        }

        static BinTree FindUncle(BinTree root, int number)
        {
            BinTree father = root;
            BinTree grandfather = root;

            if (root.data == number || root.left.data == number || root.right.data == number)
                return null;

            while (father.left.data != number && father.right.data != number)
            {
                if (number < father.data)
                {
                    grandfather = father;
                    father = father.left;
                }

                else
                {
                    grandfather = father;
                    father = father.right;
                }
            }

            if (grandfather.right == father)
                return grandfather.left;
            return grandfather.right;
        }

        static void PrintWay(BinTree root, int number)
        {
            BinTree actual = root;

            while (actual.data != number)
            {
                if (number < actual.data)
                {
                    Console.Write("{0} < ", actual.data);
                    actual = actual.left;
                }
                else
                {
                    Console.Write("{0} > ", actual.data);
                    actual = actual.right;
                }
            }

            Console.Write("{0}", actual.data);
        }

        static void CountRec(BinTree root, ref int qty)
        {
            if (root.left != null)
            {
                qty++;
                CountRec(root.left, ref qty);
            }

            if (root.right != null)
            {
                qty++;
                CountRec(root.right, ref qty);
            }
        }

        static int Count(BinTree root)
        {
            int qty = 0;
            CountRec(root, ref qty);
            return ++qty;
        }

        static BinTree Delete(BinTree root, int number)
        {
            BinTree unit = root;
            BinTree father = root;
            BinTree substitute;
            BinTree subfather;

            while (unit.data != number)
            {
                if (number < unit.data)
                {
                    father = unit;
                    unit = unit.left;
                }
                else
                {
                    father = unit;
                    unit = unit.right;
                }
            }

            if (unit.left == null && unit.right == null)
            {
                if (father.left == unit)
                    father.left = null;
                if (father.right == unit)
                    father.right = null;
                if (father == unit)
                    return null;
            }
            else if (unit.left == null || unit.right == null)
            {
                if (father.left == unit)
                    if (unit.left != null)
                        father.left = unit.left;
                    else
                        father.left = unit.right;
                if (father.right == unit)
                    if (unit.right != null)
                        father.right = unit.right;
                    else
                        father.right = unit.left;
                if (father == unit && father.left == null)
                    root = father.right;
                else if (father == unit && father.right == null)
                    root = father.left;
            }
            else
            {
                substitute = unit;
                subfather = substitute;
                substitute = substitute.left;
                bool visit = false;

                while (substitute.right != null)
                {
                    subfather = substitute;
                    substitute = substitute.right;
                    visit = true;

                }

                if (visit)
                {
                    if (substitute.left != null)
                        subfather.right = substitute.left;
                    else
                        subfather.right = null;
                    substitute.left = unit.left;
                    substitute.right = unit.right;
                }
                else
                    substitute.right = unit.right;

                if (father == unit)
                    father = root = substitute;
                else if (father.left == unit)
                    father.left = substitute; 
                else
                    father.right = substitute;
            }

            return root;
        }

        static void Main(string[] args)
        {
            int[] array = { 35, 12, 64, 5, 14, 88, 72, 17, 44, 90, 13, 99, 6, 2, 1 };
            BinTree bt1 = ConvertArray(array);
            BinTree bt2 = Find(bt1, 12);
            Print(bt1);
            BinTree uncle = FindUncle(bt1, 13);
            //PrintWay(bt1, 17);
            int count = Count(bt2);
            BinTree bt3 = Delete(bt1, 1);
            BinTree bt4 = Delete(bt3, 13);
            BinTree bt5 = Delete(bt4, 35);
            BinTree bt6 = Delete(bt5, 12);
            BinTree bt7 = Delete(bt6, 6);
            Print(bt3);
            Print(bt4);
            Print(bt5);
            Print(bt6);
            Print(bt7);
            Console.Read();
        }
    }
}
